/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.webbamboo.beams;

import javax.annotation.PostConstruct;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.io.Serializable;
/**
 *
 * @author alex
 */
@Component
@Scope("session")
public class WebBambooBean implements Serializable {
    private String mensajeBienvenida;
    
    @PostConstruct
    public void init() {
        this.mensajeBienvenida = "Bienvenidos a la página de bamboo!... pirata";
    }
    
    public String getMensajeBienvenida() {
        return this.mensajeBienvenida;
    }
}
